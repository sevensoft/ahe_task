<?php

namespace App\Jobs;

use App\User;
use App\Models\Task;
use App\Mails\TaskNotify;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;

class SendNotification
{
    use SerializesModels;

    /**
     * @var Task
     */
    private $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function handle(Mailer $mailer)
    {
        $user = User::findOrFail($this->task->user_id);

        $mailer->to($user)
            ->send(new TaskNotify($this->task, $user));
    }
}
