<?php

namespace App\Http\Middleware;

use App\Models\Task;
use Closure;

class CheckTask
{
    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param \Closure $next
     * @param int $taskId
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Task::findOrFail($request->route('id'))->isOwner()) {
            return redirect()->home();
        }

        return $next($request);
    }
}
