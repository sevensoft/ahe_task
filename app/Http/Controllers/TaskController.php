<?php

namespace App\Http\Controllers;

use App\Http\Middleware\CheckTask;
use App\Jobs\SendNotification;
use App\Models\Task;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth', ['except' => ['notification']]);
        //$this->middleware(CheckTask::class);
    }

    /**
     * Wysyłka powiadomień
     *
     * @author piofra
     */
    public function notification()
    {
        $tasks = Task::where('sending', false)
            ->whereRaw('(start - INTERVAL reminder_time MINUTE) <= NOW()')
            ->get();

        foreach ($tasks as $task) {
            try {
                $this->dispatchNow(new SendNotification($task));

                $task->sending = true;
                $task->save();
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Lista zadań użytkownika
     *
     * @return $this
     */
    public function index()
    {
        $tasks = Auth::user()->getTasks()->orderBy('sending', 'asc')->orderBy('start', 'desc')->get();

        return view('task.index')->with('tasks', $tasks);
    }

    /**
     * Kalendarz miesięczny
     *
     * @return $this
     */
    public function calendar()
    {
        $tasks = Auth::user()->jsonTasks();

        return view('task.calendar')->with('tasks', $tasks);
    }

    /**
     * Tworzenie nowego zadania
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function new()
    {
        return view('task.edit');
    }

    /**
     * Tworzenie nowego zadania - zapis danych do bazy
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $data = [
            'user_id' => Auth::id(),
            'title' => $request->title,
            'description' => $request->description,
            'start' => Carbon::parse($request->start),
            'end' => Carbon::parse($request->end),
            'sending' => false,
            'reminder_type' => 1,
            'reminder_time' => $request->reminder_time,
        ];

        Task::create($data);

        return redirect()->route('task.index');
    }

    /**
     * Edycja zadania
     *
     * @param $id
     *
     * @return $this
     */
    public function edit($id)
    {
        $this->isOwner($id);

        $task = Task::findOrFail($id);

        return view('task.edit')->with('task', $task);
    }

    /**
     * Podgląd zadania
     *
     * @param $id
     *
     * @return $this
     */
    public function show($id)
    {
        $this->isOwner($id);

        $task = Task::findOrFail($id);

        return view('task.show')->with('task', $task);
    }

    /**
     * Zapis zamian zgłoszenia
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->isOwner($id);

        $task = Task::findOrFail($id);

        $task->title = $request->title;
        $task->description = $request->description;
        $task->reminder_time = $request->reminder_time;
        $task->start = Carbon::parse($request->start);
        $task->end = Carbon::parse($request->end);
        if ($task->sending) {
            $task->sending = ($task->reminder_time <> $request->reminder_time);
        }
        $task->save();

        return redirect()->route('task.index');
    }

    /**
     * Sprawdzenie, czy user jest właścicielem
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * @author piofra
     */
    private function isOwner(int $id)
    {
        if (!Task::findOrFail($id)->isOwner()) {
            return redirect()->route('task.index');
        }
    }
}
