<?php

namespace App\Mails;

use App\Models\Task;
use App\User;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TaskNotify extends Mailable
{
    use SerializesModels;

    public $user;
    public $task;

    public function __construct(Task $task, User $user)
    {
        $this->user = $user;
        $this->task = $task;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = sprintf('Przypomnienie o wydarzeniu: %s', $this->task->title);

        return $this->subject($subject)
            ->markdown('emails.notify');
    }
}
