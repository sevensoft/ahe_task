<?php

namespace App\Console\Commands;

use App\Jobs\SendNotification;
use App\Models\Task;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

/**
 * Wywołanie: php artisan notify:send
 *
 * Class SendEmails
 * @package App\Console\Commands
 *
 * @author  piofra
 */
class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Wysyłka powiadomień do użytkowników';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tasks = Task::where('sending', false)
            ->whereRaw('(start - INTERVAL reminder_time MINUTE) <= NOW()')
            ->get();

        foreach ($tasks as $task) {
            $this->info(sprintf('Wysyłam maila dla zadania: #%d (%s)', $task->id, $task->title));

            try {
                $mail = new SendNotification($task);

                $task->sending = true;
                $task->save();
            } catch (Exception $e) {
                $this->error($e->getMessage());
            }
        }
    }
}
