@extends('layouts.app')

@section('css')
    <link href="{{url('/css/fullcalendar.min.css')}}" rel="stylesheet"/>
    <link href="{{url('/css/fullcalendar.print.min.css')}}" rel="stylesheet" media="print"/>
@endsection


@section('content')
    <div class="container">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{trans('task.calendar')}}</h3>
                </div>
                <div class="panel-body">
                    <div id='calendar'></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{url('/js/moment-with-locales.js')}}"></script>
    <script src="{{url('/js/fullcalendar.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listWeek'
                },
                locale: 'pl',
                timeFormat: 'H(:mm)',
                editable: false,
                navLinks: true, // can click day/week names to navigate views
                eventLimit: true, // allow "more" link when too many events
                events: @json($tasks),
                loading: function (bool) {
                    $('#loading').toggle(bool);
                }
            });
        });
    </script>
@endsection