<?php

return [
    'edit' => 'Edycja',
    'show' => 'Podgląd',
    'create' => 'Nowe zadanie',
    'calendar' => 'Kalendarz',

    'title' => 'Nazwa zadania',
    'description' => 'Opis',
    'start' => 'Data rozpoczęcia',
    'end' => 'Data zakończenia',
    'remind' => 'Przypomnij',
    'remind_desc' => 'min. przed wydarzeniem',

    'save' => 'Zapisz',
    'cancel' => 'Anuluj',
];