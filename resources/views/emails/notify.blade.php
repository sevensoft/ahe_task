@component('mail::message')
Witaj {{$user->name}},

otrzymujesz tego e-maila, ponieważ w kalendarzu ustawiłeś powiadomienie o tym wydarzeniu.

@component('mail::button', ['url' => route('task.show', [$task->id])])
    Zobacz w kalendarzu
@endcomponent

***
##Tytuł: {{$task->title}}
###Rozpoczęcie : {{$task->start}}
###Zakończenie : {{$task->end}}

@if($task->description)
{{$task->description}}
@endif
***

Pozdrawiamy,<br>
{{ config('app.name') }}
@endcomponent
