@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Moje zadania</h3>
                </div>
                <div class="panel-body">


                    <div class="table-responsive">
                        <div class="table-responsive">
                            <table class="table table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th>Tytuł</th>
                                    <th class="col-md-2">Data</th>
                                    <th class="col-md-2"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tasks as $task)
                                    <tr>
                                        <td>@if ($task->sending)<strike>@endif{{$task->title}}@if ($task->sending)</strike>@endif</td>
                                        <td>{{\Carbon\Carbon::parse($task->start)->format('d.m.Y H:i')}}</td>
                                        <td class="text-right">
                                            @if (!$task->sending)
                                                <a class="btn btn-default btn-xs" href="{{route('task.edit', [$task->id])}}">
                                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edycja
                                                </a>
                                            @endif
                                            <a class="btn btn-default btn-xs" href="{{route('task.show', [$task->id])}}">
                                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Podgląd
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    Ilość zadań: {{$tasks->count()}}
                </div>
            </div>
        </div>
    </div>
@endsection