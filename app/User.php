<?php

namespace App;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function getTasks()
    {
        return $this->hasMany('App\Models\Task', 'user_id', 'id');
    }

    public function jsonTasks()
    {
        $data = $this->getTasks()->get(['title', 'start', 'end', 'description']);



        return json_decode($data);
    }
}
