<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Task extends Model
{
    protected $table = 'tasks';

    protected $fillable = [
        'user_id',
        'title',
        'description',
        'start',
        'end',
        'reminder_type',
        'reminder_time',
        'sending',
    ];

    public function isOwner(): bool
    {
        return (Auth::id() == $this->user_id);
    }
}
