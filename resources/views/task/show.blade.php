@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>{{$task->title}}</strong></h3>
                </div>
                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <dt>{{trans('task.start')}}</dt>
                        <dd>{{\Carbon\Carbon::parse($task->start)->format('d.m.Y H:i')}}</dd>

                        <dt>{{trans('task.end')}}</dt>
                        <dd>{{\Carbon\Carbon::parse($task->end)->format('d.m.Y H:i')}}</dd>
                    </dl>

                    @if ($task->description)
                    <blockquote>
                        <p>{{$task->description}}</p>
                    </blockquote>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection