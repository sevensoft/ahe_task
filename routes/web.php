<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'TaskController@index')->name('task.index');

Route::get('/calendar', 'TaskController@calendar')->name('task.calendar');

Route::get('/task/new', 'TaskController@new')->name('task.new');
Route::put('/task/new', 'TaskController@create')->name('task.new');

Route::get('/task/edit/{id}', 'TaskController@edit')->name('task.edit');
Route::put('/task/edit/{id}', 'TaskController@update')->name('task.edit');

Route::get('/task/show/{id}', 'TaskController@show')->name('task.show');

Route::get('/send/notification', 'TaskController@notification')->name('send.notification');