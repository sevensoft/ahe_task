@extends('layouts.app')

@section('css')
    <link href="{{url('/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet"/>
@endsection


@section('content')
    <div class="container">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if (isset($task))
                        <h3 class="panel-title">{{trans('task.edit')}}</h3>
                    @else
                        <h3 class="panel-title">{{trans('task.create')}}</h3>
                    @endif
                </div>
                <div class="panel-body">
                    @if (isset($task))
                        <form class="form-horizontal" method="POST" action="{{route('task.edit', [$task->id])}}">
                            @else
                                <form class="form-horizontal" method="POST" action="{{route('task.new')}}">
                                    @endif
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}

                                    <div class="form-group">
                                        <label for="title" class="col-sm-2 control-label">{{trans('task.title')}}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="title" placeholder="{{trans('task.title')}}" value="{{@$task->title}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="title" class="col-sm-2 control-label">{{trans('task.description')}}</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" name="description" placeholder="{{trans('task.description')}}">{{@$task->description}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="start" class="col-sm-2 control-label">{{trans('task.start')}}</label>
                                        <div class="col-sm-9">
                                            <div class="input-group date" id="startdate">
                                                <input type="text" readonly="" class="form-control" name="start" placeholder="Czas rozpoczęcia zadania"
                                                       value="{{\Carbon\Carbon::parse(@$task->start)->format('d.m.Y H:i')}}">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="end" class="col-sm-2 control-label">{{trans('task.end')}}</label>
                                        <div class="col-sm-9">
                                            <div class="input-group date" id="enddate">
                                                <input type="text" readonly="" class="form-control" name="end" placeholder="Czas rozpoczęcia zadania"
                                                       value="{{\Carbon\Carbon::parse(@$task->end)->format('d.m.Y H:i')}}">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="end" class="col-sm-2 control-label">{{trans('task.remind')}}</label>
                                        <div class="col-sm-3">
                                            <div class="input-group" id="remind">
                                                <select name="reminder_time" class="form-control">
                                                    <option value="0" @if (@$task->reminder_time == 0) selected @endif>0</option>
                                                    <option value="5" @if (@$task->reminder_time == 5) selected @endif>5</option>
                                                    <option value="10" @if (@$task->reminder_time == 10) selected @endif>10</option>
                                                    <option value="15" @if (@$task->reminder_time == 15) selected @endif>15</option>
                                                    <option value="20" @if (@$task->reminder_time == 20) selected @endif>20</option>
                                                    <option value="30" @if (@$task->reminder_time == 30) selected @endif>30</option>
                                                </select>
                                                <div class="input-group-addon">{{trans('task.remind_desc')}}</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="btn-group pull-right" role="group">
                                        <button type="submit" class="btn btn-success">{{trans('task.save')}}</button>
                                        <button type="button" class="btn btn-danger" onclick="history.back()">{{trans('task.cancel')}}</button>
                                    </div>
                                </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{url('/js/moment-with-locales.js')}}"></script>
    <script src="{{url('/js/bootstrap-datetimepicker.min.js')}}"></script>

    <script type="text/javascript">
        $(function () {
            $('#startdate').datetimepicker({
                locale: 'pl',
                ignoreReadonly: true,
                showTodayButton: true
            });

            $('#enddate').datetimepicker({
                locale: 'pl',
                ignoreReadonly: true,
                showTodayButton: true
            });
        });
    </script>
@endsection